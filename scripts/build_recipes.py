#!/usr/bin/env python3
import argparse
import os
import subprocess
import yaml

from cpt.packager import ConanMultiPackager
from conans import tools
from conans.util.files import load
from conans.errors import ConanException
from conans import tools


class cd:
    """Context manager for changing the current working directory"""

    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def _load_yaml(filename):
    if not os.path.exists(filename):
        return None
    try:
        data = yaml.safe_load(load(filename))
    except Exception as e:
        raise ConanException("Invalid yml format at {}: {}".format(filename, e))

    return data or {}


class Builder(object):
    """Builder Conan's recipes manager"""

    _config_data = None
    _base_path = None
    _recipes = []
    _remote = None
    _remote_user = None
    _remote_password = None

    def __init__(self, file_name):
        self._config_data = _load_yaml(file_name)
        if not self._config_data:
            raise ConanException(
                "Invalid yml format at {}, don" "t exists ".format(file_name)
            )

        if "base_path" in self._config_data["recipes"]:
            self._base_path = os.path.join(
                os.getcwd(), self._config_data["recipes"]["base_path"]
            )
        else:
            self._base_path = os.getcwd()

        if "recipes" in self._config_data["recipes"]:
            self._recipes = self._config_data["recipes"]["recipes"]

        print("Loading the {} configuration file ".format(file_name))

    def set_remote(self, remote, remote_user, remote_password):
        self._remote = remote
        self._remote_user = remote_user
        self._remote_password = remote_password

    def run(self):
        for recipe_name in self._recipes:
            with cd(os.path.join(self._base_path, recipe_name)):
                self.build_packages(recipe_name)

    def build_packages(self, recipe_name):
        config = _load_yaml("config.yml")
        versions = config["versions"]
        for version in versions:
            if "@" in version:
                reference = f"{recipe_name}/{version}"
            else:
                reference = f"{recipe_name}/{version}@_/_"
            self.build_package( reference, versions[version]["folder"])

    def build_package(self, reference, folder):
        with cd(os.path.join(os.getcwd(), folder)):
            builder = ConanMultiPackager(
                reference=reference,
                username=self._remote_user,
                password=self._remote_password,
                upload=self._remote
            )
            builder.add_common_builds()
            builder.run()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-y", "--yaml", help="Yaml configure file")
    parser.add_argument("-r", "--remote", help="Remote artifactory server address")
    parser.add_argument(
        "-u",
        "--remote_user",
        help="Username you want to use. If no name is provided it will show the current user",
    )
    parser.add_argument(
        "-p",
        "--remote_password",
        help="User password. Use double quotes if password with spacing,and escape quotes "
        "if existing. If empty, the password is requested interactively (not exposed)",
    )

    os.environ["CONAN_SYSREQUIRES_MODE"] = "enabled"

    args = parser.parse_args()

    yaml_path = args.yaml if args.yaml else "build"

    builder = Builder(yaml_path)

    if args.remote and args.remote_user:
        builder.set_remote(args.remote, args.remote_user, args.remote_password)

    builder.run()
